package swiftstroke;

import java.util.LinkedList;
import java.util.List;

public class Character {
	private List<Stroke> strokes;
	private char character;
	
	public Character(char c){
		strokes = new LinkedList<Stroke>();
		character = c;
	}
	
	public void addStroke(Stroke stroke){
		strokes.add(stroke);
	}
	public Stroke getStroke(Integer pos){
		return strokes.get(pos);
	}
	
	public char getCharacter(){
		return character;
	}
	public void setCharacter(char c){
		character = c;
	}
	
	public Integer getNumberOfStrokes(){
		return strokes.size();
	}

}
