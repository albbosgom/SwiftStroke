package swiftstroke;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PFont;
import processing.core.PShape;

public class Inspector {
	PApplet parent;
	PFont font;
	PShape character;
	PShape stroke;
	
	public Inspector(PApplet _parent){
		parent = _parent;
		font = parent.createFont("MS Gothic", 500);
		character = parent.createShape(PConstants.GROUP);
		stroke = parent.createShape(PConstants.GROUP);
	}

	public void learnCharacter(Character chara) {
		// TODO Auto-generated method stub
		parent.fill(255);
		parent.textFont(font,500);
		parent.textAlign(PConstants.CENTER, PConstants.CENTER);
		parent.text(chara.getCharacter(), parent.width/2, parent.height/2); 
		parent.fill(0);
		parent.ellipse(parent.mouseX, parent.mouseY, 10,10);
		if (parent.mousePressed == true) {
				parent.stroke(150);
				parent.strokeCap(PConstants.ROUND);
				parent.strokeWeight(60);
			    PShape line = parent.createShape(PConstants.LINE, parent.mouseX, parent.mouseY, parent.pmouseX, parent.pmouseY);
			    stroke.addChild(line);			
		}
		parent.blendMode(PConstants.DARKEST);
		parent.shape(character);
		parent.shape(stroke);
		parent.fill(255);
		parent.stroke(255);
		parent.blendMode(PConstants.NORMAL);
		
	}
	
	public void cleanStroke(){
		stroke = parent.createShape(PConstants.GROUP);
	}
	
	public void addStroke(){
		character.addChild(stroke);
		cleanStroke();
	}
	
	public void reset(){
		character = parent.createShape(PConstants.GROUP);
		stroke = parent.createShape(PConstants.GROUP);		
	}
}
