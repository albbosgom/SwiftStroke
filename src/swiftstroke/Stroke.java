package swiftstroke;

import java.util.LinkedList;
import java.util.List;

import info.debatty.java.stringsimilarity.NormalizedLevenshtein;

public class Stroke {
	
	@Override
	public String toString() {
		return "Stroke order is " + order;
	}

	private List<Integer> order;
	private Double acceptanceRatio;
	
	public Stroke(){
		order = new LinkedList<Integer>();
		acceptanceRatio = 0.0;
	}
	
	public double getAcceptanceRatio(){
		return acceptanceRatio;		
	}
	
	public void updateRatio(Stroke stroke){
		/* We get the normalized distance between the two strokes*/
		Double normalizedDistance=getDifference(order, stroke.getOrder());
		/*
		 * Since the current stroke and the stroke that was made are supposedly
		 * equals, we update the threshold so that it accepts the new error.
		 */
		if(acceptanceRatio<normalizedDistance)
			acceptanceRatio=normalizedDistance;
	}
	
	private Double getDifference(List<Integer> stroke1, List<Integer> stroke2) {
		/*
		 * We get the Normalized Levenshtein distance. That is, the levenshtein
		 * distance divided by the length of the longer string. This means that
		 * a distance of 1 is a completely arbitrary string, and a distance of 
		 * 0 is exactly the same string.
		 */
		NormalizedLevenshtein l = new NormalizedLevenshtein();
		return l.distance(stroke1.toString(), stroke2.toString());
	}

	public List<Integer> getOrder(){
		return order;
	}
	
	public Integer getSize(){
		return order.size();
	}
	
	
	public void addBlock(Integer block){
		/*
		 * Since this function is constantly working, we only add the new
		 * block if it's different from the previous one.
		 */
		if(getSize()==0) order.add(block);
		else if(!order.get(getSize()-1).equals(block)) 
		order.add(block);
	}
	
	public boolean equals(Stroke stroke){
		/*
		 * Well, this is pretty obvious. If the normalized distance is less
		 * than the acceptance error defined for the current stroke, it's
		 * theoretically the same stroke (or a really similar one, if properly trained).
		 */
		Double similarity = getDifference(order, stroke.getOrder());
		return similarity<=getAcceptanceRatio();
	}
	
	

}
