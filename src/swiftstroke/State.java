package swiftstroke;

public enum State {
	List, 
	Edit,
	EditCharacter,
	Input, 
	Training, 
	Checking, 
	Learning
}
