package swiftstroke;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.LinkedList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonSyntaxException;
import com.google.gson.reflect.TypeToken;
import com.google.gson.stream.JsonReader;

import controlP5.ControlFont;
import controlP5.ControlP5;
import processing.core.PApplet;
import processing.core.PFont;
import processing.core.PShape;
import processing.data.JSONObject;

public class SwiftStroke extends PApplet {
	
	PShape character;
	JSONObject json;
	boolean training;
    ControlP5 cp5;
        
	public ControlFrame cf;
	public void settings(){
		size(400,400, JAVA2D);
		
	}
	public void setup() {
		background(0);
		surface.setTitle("Swift Stroke");
		character = createShape(GROUP);
		training = true;
		surface.setResizable(false);
		cp5 = new ControlP5(this);
		// create a new button with name 'buttonA'
		cp5.addButton("loadFile")
			.setCaptionLabel("Load a set to learn")
			.setPosition(100,100)
			.setSize(200,50)
			.setColorBackground(color(25,25,25))
			.setColorForeground(color(100,100,100))
			.setColorActive(color(10,10,10))
			.activateBy(ControlP5.RELEASE);
			;
		cp5.addButton("newSet")
		.setCaptionLabel("Create a new Set")
		.setPosition(100,150)	    
		.setSize(200,50)
		.setColorBackground(color(25,25,25))
		.setColorForeground(color(100,100,100))
		.setColorActive(color(10,10,10))
		.activateBy(ControlP5.RELEASE);
		;	
		

		PFont pfont = createFont("Montserrat",20,true); // use true/false for smooth/no-smooth
		ControlFont font = new ControlFont(pfont,241);
		cp5.getController("loadFile")
		.getCaptionLabel()
		.setFont(font)
	    .toUpperCase(false)
	    .setSize(16);
		cp5.getController("newSet")
		.getCaptionLabel()
		.setFont(font)
	    .toUpperCase(false)
	    .setSize(16);

	}

	public void draw() {
		//background(23,120,213);
		background(50);
	}
	

	public void loadFile(int value){
	// This is the place for the code, that is activated by the button
		selectInput("weno weno", "fileSelected");
	}
	

	public void newSet(int value){
	// This is the place for the code, that is activated by the button
		ControlFrame.set = new LinkedList<Character>();
		cf = new ControlFrame(this,1280,720,"box");
		
	}
	
	public void fileSelected(File selection) {
		  if (selection == null) {
		    println("Window was closed or the user hit cancel.");
		  } else {
			  try(JsonReader reader = new JsonReader(new FileReader(selection))){

				    Gson gson = new GsonBuilder().setPrettyPrinting().create();		
				    Type listType = new TypeToken<LinkedList<Character>>(){}.getType();
				    ControlFrame.set = gson.fromJson(reader, listType);
					cf = new ControlFrame(this,1280,720,"box");
			  }  
			  catch (FileNotFoundException e) {				
				  javax.swing.JOptionPane.showMessageDialog(null, "The file was not found. If you are not expecting\nthis error plase contact with the developer.", "File not found.", javax.swing.JOptionPane.INFORMATION_MESSAGE  );
			} catch (IOException e) {
				javax.swing.JOptionPane.showMessageDialog(null, "Couldn't open the file. Please check the file permissions and try again.", "Problem with the file.", javax.swing.JOptionPane.INFORMATION_MESSAGE  );
			}catch (JsonSyntaxException e){
				javax.swing.JOptionPane.showMessageDialog(null, "The selected file was invalid. Please try again or edit it.\nIf you were not expecting this error plase contact with the developer.", "Wrong file", javax.swing.JOptionPane.INFORMATION_MESSAGE  );
			}
		  }
	}
	
	
	
	public static void main(String _args[]) {
		PApplet.main(new String[] { swiftstroke.SwiftStroke.class.getName() });
		 
	}	
	
}
