package swiftstroke;

import java.util.List;

import processing.core.PApplet;
import processing.core.PFont;

public class Manager {
	PApplet parent;
	Integer position;
	List<Character> set;
	PFont font;
	
	public Manager(PApplet _parent, List<Character> set){
		parent = _parent;
		this.set = set;
		position = 1;
		font = parent.createFont("MS Gothic", 100);
	}
	
	public void showList(){
		parent.getSurface().setSize(1280, 720);
		for(int i = 1; i<20;i++){
			Character character = set.get(i*position);
			if(character == null){
				break;
			}else{
				parent.textFont(font,100);
				parent.text(character.getCharacter(), 20*i, 10*i);
			}
		}
		
	}
}
