package swiftstroke;

import java.awt.Font;
import java.io.File;
import java.util.List;

import controlP5.Button;
import controlP5.ControlFont;
import controlP5.ControlP5;
import g4p_controls.G4P;
import g4p_controls.GEditableTextControl;
import g4p_controls.GEvent;
import g4p_controls.GTextField;
import processing.core.PApplet;
import processing.core.PFont;
import wiiusej.WiiUseApiManager;
import wiiusej.Wiimote;

public class ControlFrame extends PApplet {

	int w, h, page, index;
	boolean tutorial;
	PApplet parent;
	ControlP5 cp5;
	String s;
	PFont font, smallFont;
	State state;
	String message;
	GTextField inputField, strokeField;
	Integer numberOfStrokes;
	Integer i, pos;
	Stroke stroke;
	Character chara;
	static List<Character> set;
	Trainer trainer;
	Wiimote wiimote;
	Inspector inspector;
	Button nextButton, newButton, previousButton, editButton, backButton, startButton, deleteButton, saveToFileButton, saveButton, reSync;

	public ControlFrame(PApplet _parent, int _w, int _h, String _name) {
		super();
		parent = _parent;
		w = _w;
		h = _h;
		PApplet.runSketch(new String[] { this.getClass().getName() }, this);
	}

	public void settings() {
		size(w, h, JAVA2D);
	}

	public void setup() {
		surface.setTitle("Swift Stroke - Entrenador");
		try{
			Wiimote[] wiimotes = WiiUseApiManager.getWiimotes(1, true);
			wiimote = wiimotes[0];
			wiimote.addWiiMoteEventListeners(new WiimoteController());
		}catch(Exception e){
			wiimote = null;
		}
		font = createFont("M+ 1c regular", 70);
		smallFont = createFont("M+ 1c regular", 20);
		cp5 = new ControlP5(this);
		trainer = new Trainer(this);
		inspector = new Inspector(this);
		setUpControlP5(cp5);
		tutorial = true;
		state = State.List;
		s = "";
		message = "";
		page = 0;
		index = 0;
		stroke = new Stroke();
		numberOfStrokes = 3;
		i = 0;
		if(!set.isEmpty())
			for (int j = 0; j<36;j++) {		

				try {
					Character character = set.get(j + 36 * page);
					s += character.getCharacter() + "   ";
				} catch (IndexOutOfBoundsException e) {
					nextButton.hide();
					break;
				}
			}

	}

	private void setUpControlP5(ControlP5 cp5) {
		
		previousButton = cp5.addButton("previous")
				.setPosition(width / 2 - 500, height - 75)
				.setSize(200, 50)
				.hide()
				.setCaptionLabel("Previous Page")
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		newButton = cp5.addButton("newCharacter")
				.setPosition(width / 2 - 250, height - 75)
				.setSize(200, 50)
				.setCaptionLabel("New Character")
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		editButton = cp5.addButton("edit")
				.setCaptionLabel("Edit")
				.setPosition(width / 2, height - 75)
				.setSize(200, 50)				
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		if (set.size() <= 0) {
			editButton.hide();
		}
		nextButton = cp5.addButton("next")
				.setCaptionLabel("Next Page")
				.hide()
				.setPosition(width / 2 + 250, height - 75)
				.setSize(200, 50)
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		if(set.size()>36)
			nextButton.show();
		
		inputField = new GTextField(this, 520, 190, 70, 80, G4P.SCROLLBARS_NONE);
		inputField.setFont(new Font("M+ 2c regular", Font.PLAIN, 70));
		inputField.setOpaque(true);
		inputField.setVisible(false);
		
		strokeField = new GTextField(this, 520, 300, 120, 80, G4P.SCROLLBARS_NONE);
		strokeField.setFont(new Font("M+ 2c regular", Font.PLAIN, 70));
		strokeField.setOpaque(true);
		strokeField.setVisible(false);
		
		saveButton = cp5.addButton("save")
				.setCaptionLabel("Save")
				.hide()
				.setPosition(20, 200)
				.setPosition(width / 2, height - 75)
				.setSize(200, 50)
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		backButton = cp5.addButton("back")
				.setCaptionLabel("Back")
				.hide()
				.setPosition(width / 2, 30)
				.setSize(200, 50)
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		saveToFileButton = cp5.addButton("saveToFile")
				.setCaptionLabel("Save Set to File")
				.setPosition(width / 2 - 250, 30)
				.setSize(200, 50)
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		reSync = cp5.addButton("reSync")
				.setCaptionLabel("Resync Wiimote")
				.setPosition(width / 2 + 250, 30)
				.setSize(200, 50)
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		startButton = cp5.addButton("start")
				.setCaptionLabel("Start")
				.setPosition(width / 2 - 250, height - 75)
				.setSize(200, 50)
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));
		deleteButton = cp5.addButton("delete")
				.setCaptionLabel("Delete")
				.setPosition(width / 2 - 250, height - 75)
				.setSize(200, 50).hide()
				.setColorBackground(color(25,25,25))
				.setColorForeground(color(100,100,100))
				.setColorActive(color(10,10,10));

		PFont pfont = createFont("Montserrat",20,true);
		ControlFont buttonFont = new ControlFont(pfont,241);
		
		/* Now we adjust the font and size for all buttons*/
		previousButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		nextButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		editButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		newButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		saveButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		saveToFileButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		reSync.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		startButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		deleteButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		backButton.getCaptionLabel().setFont(buttonFont).toUpperCase(false).setSize(16);
		
		
		if (set.isEmpty())
			startButton.hide();

	}
	public void handleTextEvents(GEditableTextControl source, GEvent event) { 
		if(event == GEvent.ENTERED){
			// automatically receives results from controller input
			beginTraining();
			
		} 
	}


	private void beginTraining() {
		reloadText();
		tutorial = true;
		numberOfStrokes = Integer.parseInt(strokeField.getText());
		chara = new Character(inputField.getText().charAt(0));
		i=0;
		state = State.Training;
		reloadGUI(State.Training);
		
	}

	public void draw() {
		blendMode(NORMAL);
		background(50);
		
		if (state == State.Training) {
			trainer.startTraining(chara);
			fill(255);
			textFont(smallFont);
			textAlign(LEFT);
			text(message, 20, height - 20);
		} else if (state == State.Checking) {
			textFont(smallFont);
			textAlign(LEFT);
			text(message, 20, height - 70);
			inspector.learnCharacter(chara);

		} else if (state == State.Learning) {
			textFont(smallFont);
			textAlign(LEFT);
			text(message, 20, height - 20);
			inspector.learnCharacter(chara);
		} else if (state == State.EditCharacter){
			textSize(72);
			textFont(font);
			textAlign(CENTER);
			fill(255);
			text(chara.getCharacter(), width/2, height/2);
		}
		else if(state == State.Input){
			PFont pfont = createFont("Montserrat",40,true);
			textAlign(LEFT);
			textFont(pfont);
			fill(255);
			text("Character:", 110, 250);
			textFont(pfont);
			fill(255);
			text("Number of Strokes:", 110, 355);
			
		}
		
		else {
			textSize(32);
			textFont(font);
			textAlign(LEFT);
			fill(255);
			text(s, 100, 100, 1080, 520);
		}
	}

	public void next(int value) {
		page++;
		s = "";
		previousButton.show();
		for (int i = 0; i < 36; i++) {
			try {
				Character character = set.get(i + 36 * page);
				s += character.getCharacter() + "   ";
			} catch (IndexOutOfBoundsException e) {
				nextButton.hide();
				break;
			}
		}
	}

	public void start(int value) {
		if(state == State.Input){
			beginTraining();
		}
		else{
			tutorial = true;
			reloadText();
			reloadGUI(State.Learning);
			state = State.Learning;
		}
	}
	
	public void reSync(int value){
		syncWiimote();
	}

	public void edit(int value) {		
		reloadGUI(State.Edit);
		if(state == State.Edit || state == State.EditCharacter){
			state = State.Input;
			reloadGUI(State.Input);
		}
		else
			state = State.Edit;

	}

	public void save(int value) {
		if(pos == null)
			set.add(chara);		
		else{
			set.set(pos, chara);
			pos = null;
		}
		back(value);
	}

	public void back(int value) {
		
		pos = null;
		index = 0;
		if (state == State.Training) { 
			if(set.isEmpty())
				s="";
			else
				for (Character character : set) {
					s += character.getCharacter() + "   ";
				}
		}
		if (state == State.List) {
			reloadGUI(State.List);

		}
		if (state == State.Training || state == State.Checking || state == State.Learning || state == State.Input) {
			state = State.List;
			reloadGUI(State.List);

		}
		
		if(state == State.Edit){
			state = State.List;
			reloadGUI(State.List);
		}
		
		if(state == State.EditCharacter){
			state = State.Edit; // ????
			reloadGUI(State.Edit);
		}

	}

	public void saveToFile(int value) {
		selectOutput("weno weno", "fileSelected");
	}

	public void fileSelected(File selection) {
		if (selection == null) {
			println("Window was closed or the user hit cancel.");
		} else {
			println("User selected " + selection.getAbsolutePath());
			trainer.finishTraining(set, selection.getAbsolutePath());
		}
	}


	public void newCharacter(int value) {
		reloadGUI(State.Input);	
		state = State.Input;
		
	}
	
	public void reloadText(){		
		s="";
		message = "";
		if(!set.isEmpty())
			for (Character character : set) {
				s += character.getCharacter() + "   ";
			}
	}
	
	public void reloadGUI(State nextState){
		switch(nextState){
			case List:
				if (set.size() >= 36)
					nextButton.show();
				newButton.hide();				
				editButton.show();
				if(!set.isEmpty())
					startButton.show();
				inputField.setVisible(false);
				strokeField.setVisible(false);
				saveButton.hide();
				backButton.hide();
				deleteButton.hide();
				reloadText();
				break;
	
			case Edit:
				reloadText();
				if (set.size() >= 36)
					nextButton.show();
				newButton.show();
				editButton.show();
				startButton.hide();
				inputField.setVisible(false);
				strokeField.setVisible(false);
				backButton.show();
				editButton.hide();
				deleteButton.hide();
				//state = State.List;
				break;
	
			case EditCharacter:
				backButton.show();
				newButton.hide();
				deleteButton.show();
				editButton.show();
				break;
	
			case Training:
				nextButton.hide();
				newButton.hide();
				previousButton.hide();
				editButton.hide();
				inputField.setVisible(false);
				strokeField.setVisible(false);
				backButton.show();
				startButton.hide();
				break;
			case Input:	
				startButton.show();
				trainer = new Trainer(this);
				inspector = new Inspector(this);
				nextButton.hide();
				newButton.hide();
				previousButton.hide();
				editButton.hide();
				inputField.setVisible(true);
				strokeField.setVisible(true);
				backButton.show();
				s = "";
				strokeField.setText("");
				inputField.setText("");
				break;
	
			case Checking:
				break;
	
			case Learning:
				nextButton.hide();
				newButton.hide();
				previousButton.hide();
				editButton.hide();
				inputField.setVisible(false);
				strokeField.setVisible(false);
				backButton.show();
				startButton.hide();
				chara = set.get(index);
				numberOfStrokes = chara.getNumberOfStrokes();
				break;
	
			default:
				break;				
		}
	}

	public void previous(int value) {
		page--;
		s = "";
		nextButton.show();
		if (page == 0) {
			previousButton.hide();
		}
		for (int i = 0; i < 36; i++) {
			Character character = set.get(i + 36 * page);
			if (character != null)
				s += character.getCharacter() + "   ";
		}
	}
	

	public void delete(int value) {
		set.remove(chara);
		state = State.List;
		reloadGUI(state);
	}
	
	
	public void mouseReleased() {
		if (state == State.Training) {
			if(!tutorial){
				if (i == 0) {
					chara.addStroke(stroke);
					stroke = new Stroke();
					numberOfStrokes--;
				} else {
					chara.getStroke(chara.getNumberOfStrokes() - numberOfStrokes).updateRatio(stroke);
					stroke = new Stroke();
					numberOfStrokes--;
				}
				message = "Remaining strokes: " + numberOfStrokes;
				if (numberOfStrokes == 0) {
					i++;
					if (i >= 3) {
						state = State.Checking;
						saveButton.show();
					}
					message = "Good job! Now repeat it " + (3 - i) + " more time(s).";
					numberOfStrokes = Integer.parseInt(strokeField.getText());
					trainer = new Trainer(this);
	
				}
				stroke(255);
			}
			else{
				tutorial = false;
			}

		}
		if (state == State.Checking || state == State.Learning) {
			if (chara != null) {
				Integer currentStroke = chara.getNumberOfStrokes() - numberOfStrokes;
				if (chara.getStroke(currentStroke).equals(stroke)) {		
					Integer pos = currentStroke++;
					message = "Good job! The stroke number " + pos + " is correct.";
					inspector.addStroke();
					numberOfStrokes--;
					if (numberOfStrokes <= 0) {
						if (state == State.Learning) {
							index++;
							try {
								chara = set.get(index);
							} catch (Exception e) {
								tutorial = true;
							}
						}
						numberOfStrokes = chara.getNumberOfStrokes();
						if(tutorial)
							message = "Good job! You've finished. Try again or go back.";
						else
							message = "Correct! Now try the next character.";
						inspector.reset();
					}
				} else {
					if(i>=3){
						message = "You've finished! Check your new\ncharacter and don't forget to save it!";
						i=0;
					}
					else if(tutorial){
						message = "Please write the first stroke.";
						inspector.cleanStroke();
						tutorial = false;
						
					}
					else{
						message = "The last stroke was wrong! Please try again.";
						inspector.cleanStroke();
					}
				}
				stroke = new Stroke();
			}
		}
		if(state == State.Edit){
			Integer charaWidth = (width-190)/12;
			Integer charaHeight = ((height-300)/4);
			Integer posX = floor((mouseX-100)/charaWidth);
			Integer posY = floor((mouseY-100)/charaHeight);
			if(width-100 >=0 & posX<12 && height-100>=0 && posY<4 ){
				try{
					pos = posX+posY*12;
					chara = set.get(pos);
					state = State.EditCharacter;
					reloadGUI(State.EditCharacter);
					state = State.EditCharacter;
				}
				catch(Exception e){
					System.out.println("No character there. Click elsewhere.");					
				}
			}
		}

	}


	public void syncWiimote(){	
		 if(wiimote !=null){
			 wiimote.getWiiMoteEventListeners()[0].onDisconnectionEvent(null);
		 }
		 try{
			 Wiimote[] wiimotes = WiiUseApiManager.getWiimotes(1, true);
			 wiimote = wiimotes[0];
			 wiimote.activateIRTRacking();
			 wiimote.setScreenAspectRatio169();
			 wiimote.setSensorBarBelowScreen();
			 wiimote.setVirtualResolution(displayWidth , displayHeight);
			 wiimote.addWiiMoteEventListeners(new WiimoteController());
		 } catch(Exception e){
			 wiimote = null;
		 }
	}
	

	public void mouseDragged() {
		if (state == State.Training || state == State.Checking || state == State.Learning)
			stroke.addBlock(trainer.currentBlock());
	}

	public void exit() {
	}

}