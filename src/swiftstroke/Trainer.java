package swiftstroke;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import processing.core.PApplet;
import processing.core.PConstants;
import processing.core.PFont;
import processing.core.PShape;

public class Trainer {
	PApplet parent;
	PShape character;
	PFont font;
	public Trainer(PApplet _parent){
		parent = _parent;
		character = parent.createShape(PConstants.GROUP);
		font = parent.createFont("MS Gothic", 500);
	}
	
	
	public void startTraining(Character chara){		
		parent.fill(255);
		parent.textFont(font,500);
		parent.textAlign(PConstants.CENTER, PConstants.CENTER);
		parent.text(chara.getCharacter(), parent.width/2, parent.height/2); 
		parent.pushMatrix();
		parent.fill(0);
		parent.ellipse(parent.mouseX, parent.mouseY, 10,10);
		parent.popMatrix();
		if (parent.mousePressed == true) {
			if (parent.mouseButton == PConstants.RIGHT) character = parent.createShape(PConstants.GROUP);
			else{
				parent.strokeCap(PConstants.ROUND);
				parent.strokeWeight(60);
			    PShape line = parent.createShape(PConstants.LINE, parent.mouseX, parent.mouseY, parent.pmouseX, parent.pmouseY);
			    character.addChild(line);
			}
		}
		parent.blendMode(PConstants.DARKEST);
		parent.stroke(150);
		parent.shape(character);
		parent.blendMode(PConstants.NORMAL);
	}
	
	public Integer currentBlock() {
		Double blockWidth = parent.width/5.0;
		Double blockHeight = parent.height/5.0;
		Integer currentWidth = PApplet.floor((float) (parent.mouseX/blockWidth));
		Integer currentHeight = PApplet.floor((float) (parent.mouseY/blockHeight));
		return currentWidth+5*currentHeight;
	}
	
	public void finishTraining(List<Character> characters, String path){
		try (Writer writer = new FileWriter(path)) {
		    Gson gson = new GsonBuilder().setPrettyPrinting().create();
		    gson.toJson(characters, writer);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
